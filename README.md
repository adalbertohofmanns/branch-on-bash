# README #

Arquivo .sh para exibir a branch atual e a versão do Ruby do projeto.

### Oque ele faz? ###

* Backup
* Ele simplesmente faz uma cópia do seu arquivo .bash_profile para .bash_profile-old, que se encontra no diretório raiz do usuário(unix).

### Como eu o instalo? ###

* Salve-o na raiz do usuário, se preferir em outro diretorio.
* [Download Aqui](https://bitbucket.org/adalbertohofmann/branchonbash/raw/442a1fb9a7d447570dd94e658347dc3245fbc789/branchOnBash.sh)
* Abra o Terminal, Ctrl+Alt+T.
* Digite: sh branchOnBash.sh
* Pronto, reinicie o Terminal para garantir as mudanças.

![terminal.png](https://bitbucket.org/repo/g4k8yK/images/3929008550-terminal.png)

### Ajustando o Terminal ###

* Clique em `Editar`
* Preferencias do Perfil
* Na aba `Comando`
* Habilite o `Executar comando como shell da sessão`

![terminal.png](https://bitbucket.org/repo/g4k8yK/images/915668319-terminal.png)